/*!
 *
 * \file OTverifyMPASSAdataWord.h
 * \brief OTverifyMPASSAdataWord class
 * \author Fabio Ravera
 * \date 07/03/24
 *
 */

#ifndef OTverifyMPASSAdataWord_h__
#define OTverifyMPASSAdataWord_h__

#include "tools/Tool.h"
#include <map>
#ifdef __USE_ROOT__
// Calibration is not running on the SoC: I need to instantiate the DQM histogrammer here
#include "DQMUtils/DQMHistogramOTverifyMPASSAdataWord.h"
#endif
#include "tools/OTverifyCICdataWord.h"

class PatternMatcher;

class OTverifyMPASSAdataWord : public OTverifyCICdataWord
{
  public:
    OTverifyMPASSAdataWord();
    ~OTverifyMPASSAdataWord();

    void Initialise(void);

    // State machine
    void Running() override;
    void Stop() override;
    void ConfigureCalibration() override;
    void Pause() override;
    void Resume() override;
    void Reset();

    static std::string fCalibrationDescription;

  protected:
    uint8_t                fFirstStrip{15};
    uint8_t                fStripGap{10};
    uint8_t                fStubRowCoordinate{0x0A};
    std::map<int, uint8_t> fBendingToCode{{0, 5}};

    virtual std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> produceMatchingPixelClusterList(uint8_t colCoordinate);
    virtual std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> produceStripClusterList();
    virtual void                                               matchAllPossibleStubPatterns(uint8_t                                        numberOfBytesInSinglePacket,
                                                                                            size_t                                         numberOfLines,
                                                                                            std::vector<std::pair<PatternMatcher, float>>& thePatternAndEfficiencyList,
                                                                                            const std::vector<uint32_t>&                   concatenatedStubPackage,
                                                                                            Ph2_HwDescription::ReadoutChip*                theMPA);
    virtual void                                               setStubLogicParameters(Ph2_HwDescription::ReadoutChip* theMPA);
    DetectorDataContainer                                      fPatternMatchingEfficiencyContainer;

  private:
    void fillHistograms();
    void injectStubsPS(Ph2_HwDescription::ReadoutChip* theMPA, uint8_t chipIdForCIC, Ph2_HwInterface::D19cFWInterface* theFWInterface, uint8_t numberOfBytesInSinglePacket) override;
    void injectL1PS(Ph2_HwDescription::ReadoutChip* theMPA, uint8_t chipIdForCIC, Ph2_HwInterface::D19cFWInterface* theFWInterface, uint8_t numberOfBytesInSinglePacket) override;
    virtual std::vector<std::vector<std::tuple<uint8_t, uint8_t, int>>> producePossibleStubVectorList(const std::vector<std::tuple<uint8_t, uint8_t, uint8_t>>& thePixelClusterList);
    PatternMatcher produceStubPatternMatcher(const std::vector<std::tuple<uint8_t, uint8_t, int>>& theStubVector, uint8_t numberOfBytesInSinglePacket, uint8_t chipIdForCIC);
    PatternMatcher produceL1PatternMatcher(const std::vector<std::tuple<uint8_t, uint8_t, uint8_t>>& thePixelClusterList,
                                           const std::vector<std::tuple<uint8_t, uint8_t, uint8_t>>& theStripClusterList,
                                           uint8_t                                                   numberOfBytesInSinglePacket,
                                           uint8_t                                                   chipIdForCIC);

#ifdef __USE_ROOT__
    // Calibration is not running on the SoC: Histogrammer is handeld by the calibration itself
    DQMHistogramOTverifyMPASSAdataWord fDQMHistogramOTverifyMPASSAdataWord;
#endif
};

#endif
